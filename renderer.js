
const fileInput = document.querySelector('#fileInput');
fileInput.addEventListener('change', (event) => {
  const files = event.target.files;

  for (let file of files) {
    const date = new Date(file.lastModified);
    console.log(`File name :${file.name} <br />  Date : ${date} <br /> Size: ${file.size}Mo`);
  }
});
const fileWithDate = new File([], 'file.bin', {
    lastModified: new Date(2017, 1, 1),
  });
//   console.log(fileWithDate.lastModified); //returns 1485903600000
  
  const fileWithoutDate = new File([], 'file.bin');
//   console.log(fileWithoutDate.lastModified); //returns current time
  
  (function () {
    var old = console.log;
    var logger = document.getElementById('date');
    console.log = function (message) {
        if (typeof message == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(message) : message) + '<br />';
        } else {
            logger.innerHTML += message + '<br />';
        }
    }
})();

